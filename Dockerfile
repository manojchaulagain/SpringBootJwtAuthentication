FROM gradle:8.12.1 AS base
WORKDIR /usr/src
ADD . .
#RUN gradle init

FROM base AS builder
RUN gradle clean build


FROM base AS tester
RUN gradle test


FROM openjdk:17
WORKDIR /
COPY --from=builder /usr/src/build/libs/SpringBootJwtAuthentication.jar App.jar
ENTRYPOINT ["java", "-jar", "App.jar"]
EXPOSE 8080