package com.anoush.authentication.service;

import com.anoush.authentication.model.Code;

import java.util.List;
import java.util.Optional;

public interface CodeAdminService {

    Code save(Code code);

    List<Code> findAll();

    Optional<Code> findByCode(String codeId);

}
