package com.anoush.authentication.service;

import com.anoush.authentication.model.CodeSolution;
import com.anoush.authentication.repository.CodeSolutionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CodeSolutionServiceImpl implements CodeSolutionService {

    private final CodeSolutionRepository codeSolutionRepository;

    public CodeSolutionServiceImpl(CodeSolutionRepository codeSolutionRepository) {
        this.codeSolutionRepository = codeSolutionRepository;
    }

    @Override
    public CodeSolution save(CodeSolution codeSolution) {
        return codeSolutionRepository.save(codeSolution);
    }

    @Override
    public List<CodeSolution> findAll() {
        return codeSolutionRepository.findAll();
    }

    @Override
    public Optional<CodeSolution> findByCode(String codeSolutionId) {
        return codeSolutionRepository.findById(codeSolutionId);
    }

    @Override
    public void delete(String codeSolutionId) {
        codeSolutionRepository.deleteById(codeSolutionId);
    }
}
