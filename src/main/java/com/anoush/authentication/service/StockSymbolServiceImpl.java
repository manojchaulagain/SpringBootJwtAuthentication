package com.anoush.authentication.service;

import com.anoush.authentication.model.StockSymbol;
import com.anoush.authentication.repository.StockSymbolRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StockSymbolServiceImpl implements StockSymbolService {

    private final StockSymbolRepository stockSymbolRepository;

    public StockSymbolServiceImpl(StockSymbolRepository stockSymbolRepository) {
        this.stockSymbolRepository = stockSymbolRepository;
    }

    @Override
    public Optional<StockSymbol> findBySymbol(String symbol) {
        return stockSymbolRepository.findBySymbol(symbol);
    }

    @Override
    public StockSymbol save(StockSymbol stockSymbol) {
        return stockSymbolRepository.save(stockSymbol);
    }

    @Override
    public List<StockSymbol> findAll() {
        return stockSymbolRepository.findAll();
    }
}
