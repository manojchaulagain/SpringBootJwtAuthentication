package com.anoush.authentication.service;

import com.anoush.authentication.model.CodeSolution;

import java.util.List;
import java.util.Optional;

public interface CodeSolutionService {

    CodeSolution save(CodeSolution codeSolution);

    List<CodeSolution> findAll();

    Optional<CodeSolution> findByCode(String codeSolutionId);

    void delete(String codeSolutionId);
}
