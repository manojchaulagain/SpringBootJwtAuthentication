package com.anoush.authentication.service;

import com.anoush.authentication.model.Code;
import com.anoush.authentication.repository.CodeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CodeServiceImpl implements CodeService {

    private final CodeRepository codeRepository;

    public CodeServiceImpl(CodeRepository codeRepository) {
        this.codeRepository = codeRepository;
    }

    @Override
    public Code save(Code code) {
        return codeRepository.save(code);
    }

    @Override
    public List<Code> findAll() {
        return codeRepository.findAll();
    }

    @Override
    public Optional<Code> findByCode(String codeId) {
        return codeRepository.findById(codeId);
    }

    @Override
    public void delete(String codeId) {
        codeRepository.deleteById(codeId);
    }
}
