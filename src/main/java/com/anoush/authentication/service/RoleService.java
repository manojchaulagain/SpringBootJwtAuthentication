package com.anoush.authentication.service;

import com.anoush.authentication.model.Role;
import com.anoush.authentication.model.RoleName;

import java.util.Optional;

public interface RoleService {
    Role saveRole(Role role);

    Optional<Role> findByName(RoleName roleName);
}
