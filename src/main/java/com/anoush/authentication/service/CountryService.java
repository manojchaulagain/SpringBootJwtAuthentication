package com.anoush.authentication.service;

import com.anoush.authentication.model.Country;

import java.util.List;
import java.util.Optional;

public interface CountryService {

    Optional<Country> findByName(String countryName);

    Country save(Country country);

    List<Country> findAll();

}
