package com.anoush.authentication.service;

import com.anoush.authentication.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    User saveUser(User user);

    List<User> getAllUsers();
}
