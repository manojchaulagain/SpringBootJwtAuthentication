package com.anoush.authentication.service;

import com.anoush.authentication.model.Code;

import java.util.List;
import java.util.Optional;

public interface CodeService {

    Code save(Code code);

    List<Code> findAll();

    Optional<Code> findByCode(String codeId);

    void delete(String codeId);
}
