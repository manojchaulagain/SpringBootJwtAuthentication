package com.anoush.authentication.service;

import com.anoush.authentication.model.Code;
import com.anoush.authentication.model.CodeSolution;
import com.anoush.authentication.repository.CodeAdminRepository;
import com.anoush.authentication.repository.CodeSolutionRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CodeAdminServiceImpl implements CodeAdminService {

    private final CodeAdminRepository codeAdminRepository;
    private final CodeSolutionRepository codeSolutionRepository;

    public CodeAdminServiceImpl(CodeAdminRepository codeAdminRepository, CodeSolutionRepository codeSolutionRepository) {
        this.codeAdminRepository = codeAdminRepository;
        this.codeSolutionRepository = codeSolutionRepository;
    }

    @Override
    public Code save(Code code) {
        List<CodeSolution> savedCodeSolutions = new ArrayList<>();
        code.getCodeSolutions().forEach(codeSolution -> savedCodeSolutions.add(codeSolutionRepository.save(codeSolution)));
        code.setCodeSolutions(savedCodeSolutions);
        return codeAdminRepository.save(code);
    }

    @Override
    public List<Code> findAll() {
        return codeAdminRepository.findAll();
    }

    @Override
    public Optional<Code> findByCode(String codeId) {
        return codeAdminRepository.findById(codeId);
    }
}
