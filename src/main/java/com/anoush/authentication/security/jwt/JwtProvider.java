package com.anoush.authentication.security.jwt;

import com.anoush.authentication.model.UserPrinciple;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.util.Date;

@Slf4j
@Component
public class JwtProvider {

    @Value("${anoush.app.jwtSecret}")
    private String jwtSecret;

    @Value("${anoush.app.jwtExpiration}")
    private int jwtExpiration;

    public String generateJwtToken(Authentication authentication) {

        UserPrinciple userPrincipal = (UserPrinciple) authentication.getPrincipal();
        SecretKey secret = Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtSecret));
        return Jwts.builder().subject((userPrincipal.getUsername())).issuedAt(new Date())
                .claim("profile", userPrincipal.getAuthorities())
                .expiration(new Date((new Date()).getTime() + jwtExpiration * 1000L))
                .signWith(secret)
                .compact();
    }

    public boolean validateJwtToken(String authToken) {
        try {
            SecretKey secret = Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtSecret));
            Jwts.parser().verifyWith(secret).build().parseSignedClaims(authToken).getPayload();
            return true;
        } catch (io.jsonwebtoken.security.SignatureException e) {
            log.error("Invalid JWT signature -> Message: {0} ", e);
        } catch (MalformedJwtException e) {
            log.error("Invalid JWT token -> Message: {0}", e);
        } catch (ExpiredJwtException e) {
            log.error("Expired JWT token -> Message: {0}", e);
        } catch (UnsupportedJwtException e) {
            log.error("Unsupported JWT token -> Message: {0}", e);
        } catch (IllegalArgumentException e) {
            log.error("JWT claims string is empty -> Message: {0}", e);
        }

        return false;
    }

    public String getUserNameFromJwtToken(String token) {
        SecretKey secret = Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtSecret));
        return Jwts.parser().verifyWith(secret)
                .build()
                .parseSignedClaims(token)
                .getPayload().getSubject();
    }
}