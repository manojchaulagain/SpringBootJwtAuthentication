package com.anoush.authentication.controller;

import com.anoush.authentication.model.Role;
import com.anoush.authentication.model.RoleName;
import com.anoush.authentication.model.User;
import com.anoush.authentication.model.message.request.LoginForm;
import com.anoush.authentication.model.message.request.SignUpForm;
import com.anoush.authentication.model.message.response.JwtResponse;
import com.anoush.authentication.security.jwt.JwtProvider;
import com.anoush.authentication.service.RoleService;
import com.anoush.authentication.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 *
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;

    private final RoleService roleService;

    private final UserService userService;

    private final PasswordEncoder encoder;

    private final JwtProvider jwtProvider;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager, RoleService roleService, UserService userService, PasswordEncoder encoder, JwtProvider jwtProvider) {
        this.authenticationManager = authenticationManager;
        this.roleService = roleService;
        this.userService = userService;
        this.encoder = encoder;
        this.jwtProvider = jwtProvider;
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        return ResponseEntity.ok(new JwtResponse(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<String> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {
        if (userService.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<>("Fail -> Username is already taken!", HttpStatus.BAD_REQUEST);
        }

        if (userService.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<>("Fail -> Email is already in use!", HttpStatus.BAD_REQUEST);
        }

        // Creating user's account
        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()));

        Set<Role> roles = getSignUpRoles(signUpRequest);

        user.setRoles(roles);
        userService.saveUser(user);

        return ResponseEntity.ok().body("User registered successfully!");
    }

    private Set<Role> getSignUpRoles(SignUpForm signUpRequest) {
        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();
        strRoles.forEach(role -> {
            switch (role.toLowerCase()) {
                case "admin":
                    checkRoles(roles, RoleName.ROLE_ADMIN);
                    break;
                case "pm":
                    checkRoles(roles, RoleName.ROLE_PM);
                    break;
                case "user":
                    checkRoles(roles, RoleName.ROLE_USER);
                    break;
                default:
                    Role defaultRole = roleService.findByName(RoleName.ROLE_USER).orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                    roles.add(defaultRole);
            }
        });
        return roles;
    }

    private void checkRoles(Set<Role> roles, RoleName roleName) {
        Optional<Role> adminRole = roleService.findByName(roleName);
        if (adminRole.isEmpty()) {
            roles.add(roleService.saveRole(new Role(roleName)));
        } else {
            roles.add(adminRole.get());
        }
    }
}