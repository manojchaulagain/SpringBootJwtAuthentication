package com.anoush.authentication.controller;

import com.anoush.authentication.model.User;
import com.anoush.authentication.model.UserListDto;
import com.anoush.authentication.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
@Tag(name = "user", description = "the User API")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/test/user")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public String userAccess() {
        return ">>> User Contents!";
    }

    @GetMapping("/test/pm")
    @PreAuthorize("hasRole('PM') or hasRole('ADMIN')")
    public String projectManagementAccess() {
        return ">>> Board Management Project";
    }

    @GetMapping("/test/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public String adminAccess() {
        return ">>> Admin Contents";
    }

    @GetMapping("/users")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<UserListDto> getUsers() {
        return ResponseEntity.ok(UserListDto.builder().users(userService.getAllUsers()).build());
    }

    @Operation(security = {@SecurityRequirement(name = "bearer-key")}, summary = "Update user email.", description = "Update email of the user", tags = {"user"})
    @GetMapping("/users/{userName}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getUsers(@PathVariable("userName") String userName) {
        Optional<User> userOptional = userService.findByUsername(userName);
        return userOptional.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Operation(security = {@SecurityRequirement(name = "bearer-key")}, summary = "Update user email.", description = "Update email of the user", tags = {"user"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = User.class)))})

    @PutMapping("/users/{userName}")
//    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> updateEmail(
            @Parameter(description = "Username of the User to be updated. Cannot be empty.", required = true) @PathVariable("userName") String userName,
            @Parameter(description = "Email of the User to be updated. Cannot be empty.", required = true) @RequestParam String email) {
        Optional<User> userOptional = userService.findByUsername(userName);
        userOptional.ifPresent(user -> user.setEmail(email));
        return userOptional.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }
}