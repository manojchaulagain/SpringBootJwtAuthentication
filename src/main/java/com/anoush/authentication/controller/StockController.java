package com.anoush.authentication.controller;

import com.anoush.authentication.model.StockSymbol;
import com.anoush.authentication.service.StockSymbolService;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/stocks")
public class StockController {

    private final StockSymbolService stockSymbolService;

    @Autowired
    public StockController(StockSymbolService stockSymbolService) {
        this.stockSymbolService = stockSymbolService;
    }

    @GetMapping("/{symbol}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> getBySymbol(@PathVariable("symbol") String symbol) {
        if (Strings.isNullOrEmpty(symbol)) {
            return ResponseEntity.badRequest().build();
        }
        Optional<StockSymbol> countryOptional = stockSymbolService.findBySymbol(symbol);
        return countryOptional.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<List<StockSymbol>> getSymbols() {
        return ResponseEntity.ok(stockSymbolService.findAll());
    }
}