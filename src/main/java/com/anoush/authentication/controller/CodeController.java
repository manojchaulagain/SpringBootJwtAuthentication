package com.anoush.authentication.controller;

import com.anoush.authentication.model.Code;
import com.anoush.authentication.service.CodeService;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/code")
public class CodeController {

    private final CodeService codeService;

    @Autowired
    public CodeController(CodeService codeService) {
        this.codeService = codeService;
    }

    @GetMapping("/{codeId}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getCode(@PathVariable("codeId") String codeId) {
        if (Strings.isNullOrEmpty(codeId)) {
            return ResponseEntity.badRequest().build();
        }
        Optional<Code> countryOptional = codeService.findByCode(codeId);
        return countryOptional.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<List<Code>> getAllCode() {
        return ResponseEntity.ok(codeService.findAll());
    }
}