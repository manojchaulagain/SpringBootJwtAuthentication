package com.anoush.authentication.controller;

import com.anoush.authentication.model.*;
import com.anoush.authentication.service.CountryService;
import com.anoush.authentication.service.RoleService;
import com.anoush.authentication.service.StockSymbolService;
import com.anoush.authentication.service.UserService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.task.TaskExecutor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.IntStream;

@Component
@Slf4j
public class DataInitializer implements CommandLineRunner {

    private static final String RESOURCES_FOLDER = "src/main/resources";
    private static final String STOCKS_RESOURCES_FOLDER = RESOURCES_FOLDER + "/stocks";

    private final CountryService countryService;
    private final RoleService roleService;
    private final StockSymbolService stockSymbolService;
    private final UserService userService;

    private final PasswordEncoder passwordEncoder;
    private final TaskExecutor taskExecutor;

    @Value("${anoush.app.mongo.initialize.data}")
    private boolean load;

    @Autowired
    public DataInitializer(CountryService countryService, RoleService roleService, UserService userService, PasswordEncoder passwordEncoder, StockSymbolService stockSymbolService, @Qualifier("applicationTaskExecutor") TaskExecutor taskExecutor) {
        this.countryService = countryService;
        this.roleService = roleService;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.stockSymbolService = stockSymbolService;
        this.taskExecutor = taskExecutor;
    }

    @Override
    public void run(String... args) throws Exception {
        if (load) {
            log.info("Loading Data.");
            addUsers();
            addCountries();
            addStockSymbols();
        } else {
            log.info("Data loading is turned off.");
        }
    }

    private void addStockSymbols() {
        try {
            List<CsvBean> csvBeans = beanBuilder(Paths.get(STOCKS_RESOURCES_FOLDER + "/amexlist.csv"));
            csvBeans.addAll(beanBuilder(Paths.get(STOCKS_RESOURCES_FOLDER + "/nasdaqlist.csv")));
            csvBeans.addAll(beanBuilder(Paths.get(STOCKS_RESOURCES_FOLDER + "/nyselist.csv")));
            csvBeans.forEach(bean -> stockSymbolService.save((StockSymbol) bean));
        } catch (Exception e) {
            log.error("Error Occurred while adding stock symbols.", e);
        }

    }

    private List<CsvBean> beanBuilder(Path path) throws Exception {
        CsvTransfer csvTransfer = new CsvTransfer();
        HeaderColumnNameMappingStrategy<CsvBean> ms = new HeaderColumnNameMappingStrategy<>();
        ms.setType(StockSymbol.class);

        Reader reader = Files.newBufferedReader(path);
        CsvToBean<CsvBean> cb = new CsvToBeanBuilder<CsvBean>(reader).withType(StockSymbol.class).withMappingStrategy(ms).build();

        csvTransfer.setCsvList(cb.parse());
        reader.close();
        return csvTransfer.getCsvList();
    }

    private void addCountries() {
        try {
            log.info("Adding Countries.");
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Country[] countries = mapper.readValue(new File(RESOURCES_FOLDER + "/countries.json"), Country[].class);
            int i = 0;
            for (Country country : countries) {
                countryService.save(country);
                i++;
                if (i == countries.length) log.info("Added all the countries to the database.");
            }
        } catch (Exception e) {
            log.error("Error Occurred while adding countries.", e);
        }
    }

    private void addUsers() {
        try {
            log.info("Adding Roles.");
            for (RoleName roleName : RoleName.values()) {
                Role role = new Role(roleName);
                roleService.saveRole(role);
            }
            final int range = 1;
            IntStream.range(0, 1).forEach(i -> {
                int startIndex = range * i;
                taskExecutor.execute(new ThreadedDataInitializer(roleService, userService, startIndex, range, passwordEncoder));
            });
        } catch (Exception e) {
            log.error("Error Occurred while adding users.", e);
        }
    }
}