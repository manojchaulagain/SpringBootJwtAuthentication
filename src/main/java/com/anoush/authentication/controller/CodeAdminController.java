package com.anoush.authentication.controller;

import com.anoush.authentication.model.Code;
import com.anoush.authentication.service.CodeAdminService;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/admin/code")
public class CodeAdminController {

    private final CodeAdminService codeAdminService;

    @Autowired
    public CodeAdminController(CodeAdminService codeAdminService) {
        this.codeAdminService = codeAdminService;
    }

    @GetMapping("/{codeId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getCode(@PathVariable("codeId") String codeId) {
        if (Strings.isNullOrEmpty(codeId)) {
            return ResponseEntity.badRequest().build();
        }
        Optional<Code> countryOptional = codeAdminService.findByCode(codeId);
        return countryOptional.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<Code>> getAllCode() {
        return ResponseEntity.ok(codeAdminService.findAll());
    }

    @PostMapping("")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Code> addCode(@RequestBody Code code) {
        return ResponseEntity.ok(codeAdminService.save(code));
    }

    @PutMapping("")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Code> updateCode(@RequestBody Code code) {
        return ResponseEntity.ok(codeAdminService.save(code));
    }
}