package com.anoush.authentication.repository;

import com.anoush.authentication.model.Code;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodeAdminRepository extends MongoRepository<Code, String> {

}
