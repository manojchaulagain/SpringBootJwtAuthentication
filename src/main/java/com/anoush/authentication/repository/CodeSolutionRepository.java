package com.anoush.authentication.repository;

import com.anoush.authentication.model.CodeSolution;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodeSolutionRepository extends MongoRepository<CodeSolution, String> {

}
