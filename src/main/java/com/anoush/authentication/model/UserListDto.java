package com.anoush.authentication.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserListDto {

    private List<User> users;
}
