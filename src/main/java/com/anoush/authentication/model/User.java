package com.anoush.authentication.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class User implements Serializable {
    @Id
    @Schema(description = "Unique identifier of the User.", example = "asdf-asdf-asdf-asdf-asdf", requiredMode = Schema.RequiredMode.REQUIRED)
    private String id;

    @Schema(description = "Name of the user.", example = "Jessica Abigail", requiredMode = Schema.RequiredMode.REQUIRED)
    private String name;

    @Schema(description = "Username of the user.", example = "manoj.chaulagain0", requiredMode = Schema.RequiredMode.REQUIRED)
    private String username;

    @Schema(description = "Email of the user.", example = "manoj.chaulagain0@gmail.com", requiredMode = Schema.RequiredMode.REQUIRED)
    private String email;

    @JsonIgnore
    private String password;

    private Set<Role> roles = new HashSet<>();

    public User(String name, String username, String email, String password) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public User(String name, String username, String email, String password, Set<Role> roles) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }
}