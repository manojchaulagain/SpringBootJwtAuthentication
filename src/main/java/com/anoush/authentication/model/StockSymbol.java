package com.anoush.authentication.model;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StockSymbol extends CsvBean {

    @Id
    private String id;
    @CsvBindByName(column = "Symbol")
    private String symbol;
    @CsvBindByName(column = "Name")
    private String name;
    @CsvBindByName(column = "LastSale")
    private String lastSale;
    @CsvBindByName(column = "MarketCap")
    private String marketCap;
    @CsvBindByName(column = "ADR TSO")
    private String adrTso;
    @CsvBindByName(column = "IPOyear")
    private String ipoYear;
    @CsvBindByName(column = "Sector")
    private String sector;
    @CsvBindByName(column = "Industry")
    private String industry;
    @CsvBindByName(column = "Summary Quote")
    private String summaryQuote;
}
