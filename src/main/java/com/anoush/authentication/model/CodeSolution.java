package com.anoush.authentication.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CodeSolution implements Serializable {

    @Id
    private String id;
    private String codeId;
    private String title;
    private String description;
    private int solutionIndex;
    private List<String> tags = new ArrayList<>();
    private String solution;

    private Date createdDate;
    private Date updatedDate;

    private String createdBy;
    private String updatedBy;
}
