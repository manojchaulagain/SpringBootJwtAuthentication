package com.anoush.authentication;

import com.anoush.authentication.service.CodeAdminService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpringBootJwtAuthenticationApplicationTests {

    @Autowired
    private CodeAdminService codeAdminService;

    @Test
    public void contextLoads() {
        assertThat(codeAdminService).isNotNull();
    }

}
